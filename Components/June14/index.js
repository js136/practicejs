// ARROW METHOD
// EX#1
const persons = [35, 24, 41, 55, 18];
const overThirty = persons.filter((person) => person > 30);
console.log(overThirty);
//   EX# 2
let myFunction = (a, b) => a * b;
console.log(myFunction(4, 5));
//   EX# 3
const materials = ['Hydrogen', 'Helium', 'Lithium', 'Beryllium'];
console.log(materials.map((material) => material.length));
//  EX# 4
var hello;
hello = () => {
  return 'Hello World!';
};
console.log(hello());
//  EX# 5
const myEveryM = [2, 4, 5, 7, 9, 1];
const arrEveryM = myEveryM.every((num) => num < 0);
console.log(arrEveryM);
// EX# 6
let summm = (a, b) => a + b;
console.log(summm(1, 2)); // 3
// EX# 7
let age = prompt('What is your age?', 18);
let welcome = age < 18 ? () => alert('Hello!') : () => alert('Greetings!');
welcome();
// EX# 8
let numbers = [4, 2, 6, 45, 89];
numbers.sort(function (a, b) {
  return b - a;
});
console.log(numbers);
// EX# 9
const greet = () => 'Hello!';
greet();
console.log(greet());
// EX# 10
const arrayy = [1, 30, 39, 29, 10, 13];
const BelowThreshold = (currentValue) => currentValue <= 30;
console.log(arrayy.every(BelowThreshold));
